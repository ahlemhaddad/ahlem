﻿
using Data.Configurations;
using Data.CustumConventions;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class PIDBContext : DbContext
    {
        public PIDBContext() : base("name=PI")
        {

        }
        public DbSet<Candidat> Candidats { get; set; }
        public DbSet<Entreprise> Entreprises { get; set; }
        public DbSet<Formation> Formations { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<Offre> Offres { get; set; }
        public DbSet<Validation> Validations { get; set; }
        public DbSet<ProjectManager> ProjectManagers { get; set; }
        public DbSet<Interview> Interviews { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
       
            modelBuilder.Conventions.Add(new DateTime2Convention());
            modelBuilder.Configurations.Add(new CertificationConfiguration());
            modelBuilder.Configurations.Add(new CandidatConfiguration());
            modelBuilder.Configurations.Add(new EntrepriseConfiguration());
            modelBuilder.Configurations.Add(new FormationConfiguration());
            modelBuilder.Configurations.Add(new OffreConfiguration());
            modelBuilder.Configurations.Add(new ValidationConfiguration());
            modelBuilder.Configurations.Add(new ProjectManagerConfiguration());
            modelBuilder.Configurations.Add(new InterviewConfiguration());




        }


    }
}