﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class ProjectManagerConfiguration : EntityTypeConfiguration<ProjectManager>
   
    {
        public ProjectManagerConfiguration()
        {
            ToTable("ProjectManager");
            HasKey(p => p.ProjectManager_Id);



        }
    }
}
