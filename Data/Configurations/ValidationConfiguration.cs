﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
    public class ValidationConfiguration: EntityTypeConfiguration<Validation>
    {
        public ValidationConfiguration()
        {
            ToTable("Validation");
            HasKey(v => v.Validation_Id);

            HasRequired(c => c.Candidat)
              .WithMany(cat1 => cat1.Validations)
              .HasForeignKey(cc => cc.Candidat_Id)
              .WillCascadeOnDelete(true);

            HasRequired(o => o.Offre)
             .WithMany(cat2 => cat2.Validations)
             .HasForeignKey(oo => oo.Offre_Id)
             .WillCascadeOnDelete(true);

        }
    }
}
