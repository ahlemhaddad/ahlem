﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
   public class InterviewConfiguration : EntityTypeConfiguration<Interview>
    {
        public InterviewConfiguration()
        {
            ToTable("Interview");
            HasKey(i => i.Interview_Id);

            HasRequired(c => c.Candidat)
              .WithMany(cat1 => cat1.Interviews)
              .HasForeignKey(cc => cc.Candidat_Id)
              .WillCascadeOnDelete(true);

            HasRequired(o => o.ProjectManager)
             .WithMany(cat2 => cat2.Interviews)
             .HasForeignKey(oo => oo.ProjectManager_Id)
             .WillCascadeOnDelete(true);

        }
    }
}
