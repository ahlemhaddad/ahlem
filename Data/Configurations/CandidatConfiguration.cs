﻿using System;
using System.Data.Entity.ModelConfiguration;
using Domain.Entities;

namespace Data.Configurations
{
    public class CandidatConfiguration:EntityTypeConfiguration<Candidat>
    {
        public CandidatConfiguration()
        {
            ToTable("Candidat");
            HasKey(c => c.Candidat_Id);
            
         

        }
    }
}
