﻿using System;
using System.Data.Entity.ModelConfiguration;
using Domain.Entities;

namespace Data.Configurations
{
    public class EntrepriseConfiguration : EntityTypeConfiguration<Entreprise>
    {
        public EntrepriseConfiguration()
        {
            ToTable("Entreprise");
            HasKey(e => e.Entreprise_Id);
        }
    }
}
