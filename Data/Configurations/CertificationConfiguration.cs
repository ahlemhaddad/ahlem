﻿using System;
using System.Data.Entity.ModelConfiguration;
using Domain.Entities;

namespace Data.Configurations
{
    public class CertificationConfiguration:EntityTypeConfiguration<Certification>
    {

        public CertificationConfiguration()
        {
             ToTable("Cerfication");
            HasKey(c => c.Cerfication_Id);

            HasRequired(c => c.Candidat)
               .WithMany(cat => cat.Certifications)
               .HasForeignKey(cc => cc.Candidat_Id)
               .WillCascadeOnDelete(true);

        }
    }
       
}
