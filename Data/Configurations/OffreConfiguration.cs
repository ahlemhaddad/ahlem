﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configurations
{
   public class OffreConfiguration : EntityTypeConfiguration<Offre>
    {
        public OffreConfiguration()
        {
            ToTable("Offre");
            HasKey(o => o.Offre_Id);



        }
    }
}
