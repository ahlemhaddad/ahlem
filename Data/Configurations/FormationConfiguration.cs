﻿using System;
using System.Data.Entity.ModelConfiguration;
using Domain.Entities;

namespace Data.Configurations
{
    public class FormationConfiguration : EntityTypeConfiguration<Formation>
    {
        public FormationConfiguration()
        {
            ToTable("Formation");
            HasKey(f => f.Formation_Id);

            HasRequired(c => c.Candidat)
              .WithMany(cat => cat.Formations)
              .HasForeignKey(cc => cc.Candidat_Id)
              .WillCascadeOnDelete(true);

        }
    }
}
