﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Infrastructure
{
    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        private PIDBContext dataContext;

        public PIDBContext DataContext
        {
            get { return dataContext; }

        }
        public DataBaseFactory()
        {
            dataContext = new PIDBContext();
        }
        protected override void DisposeCore()
        {
            if (DataContext != null)
                DataContext.Dispose();
        }

    }
}
