﻿using System;
using Service.Pattern;
using Domain.Entities;
using Data.Infrastructure;

namespace PI.Service
{
    public class EntrepriseService : Service<Entreprise>, IEntrepriseService
    {
        static IDataBaseFactory Factory = new DataBaseFactory();
        static IUnitOfWork UTK = new UnitOfWork(Factory);
        public EntrepriseService() : base(UTK)
        {

        }

    }
}
