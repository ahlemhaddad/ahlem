﻿using System;
using Domain.Entities;
using Service.Pattern;

namespace PI.Service
{
    public interface IEntrepriseService : IService<Entreprise>
    {

    }
}
