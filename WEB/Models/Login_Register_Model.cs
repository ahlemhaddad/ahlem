﻿using System;
using Domain.Entities;

namespace WEB.Models
{
    public class Login_Register_Model
    {
        public Candidat candidat { get; set; }
        public Entreprise entreprise { get; set; }
        public Login login { get; set; }
    }
}
