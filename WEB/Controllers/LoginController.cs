﻿using Domain.Entities;
using PI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WEB.Models;

namespace WEB.Controllers
{
    public class LoginController : Controller
    {

        EntrepriseService ES;


        public LoginController()
        {
            ES = new EntrepriseService();
        }

        [NonAction]
        public void sendverificationLinkEmail(string EmailID, string ActivationCode)
        {
            var verifyurl = "/Signup/VerifiyAccount/" + ActivationCode;
            var link = Request.Url.AbsolutePath.Replace(Request.Url.PathAndQuery, verifyurl);

            var fromEmail = new MailAddress("hedibammar@gmail.com", "med hedi ben ammar");
            var toEmail = new MailAddress(EmailID);
            var FromEmailPassword = "21194754";

            string subject = "sssssssssssssssssssss";

            string body = "bbbbbbbbbbbbbbbbbbbbbbbbb" +
                "<br/><a href = '" + link + "'>" + link + "</a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, FromEmailPassword),
                Timeout = 20000
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true

            }) smtp.Send(message);
        }

        [HttpGet]
        public ActionResult LoginRegister(Login_Register_Model signup_SigninModel)
        {
            if (TempData["MessageLogin"] != null)
                ViewBag.MessageLogin = TempData["MessageLogin"].ToString();

            return View(new Login_Register_Model
            {
                candidat = new Candidat(),
                entreprise = new Entreprise(),
                login = new Login()
            });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationEntreprise(Login_Register_Model ss)
        {
            bool Status = false;
            String Message = "";
            if (ModelState.IsValid)
            {
                var x = ES.GetAll().Where(a => a.Entreprise_Email == ss.entreprise.Entreprise_Email).FirstOrDefault();
                if (x != null)
                {

                    ModelState.AddModelError("EmailExist", "Email Already Exist !! ");
                    Message = "Invalid Email already exist ";
                    return View("LoginRegister", ss);
                }




                ss.entreprise.ActivationCode = Guid.NewGuid();
                ss.entreprise.IsEmailVerified = false;

                ES.Add(ss.entreprise);
                ES.Commit();

                sendverificationLinkEmail(ss.entreprise.Entreprise_Email, ss.entreprise.ActivationCode.ToString());
                Message = " Account Activation link is in your email ID";
                Status = true;





            }
            else
            {
                Message = "Invalid";
            }

            ViewBag.Message = Message;
            ViewBag.Status = Status;
            return View("LoginRegister", ss);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginEntreprise(Login_Register_Model ss, string Returnurl = "")
        {
            string message = "";
            var x = ES.GetAll().Where(a => a.Entreprise_Email == ss.login.EmailID).FirstOrDefault();
            if (x != null)
            {
                if (ss.login.Password == x.Entreprise_Password)
                {
                    if (x.IsEmailVerified)
                    {
                        int timeout = ss.login.RemeberMe ? 52600 : 20;
                        var ticket = new FormsAuthenticationTicket(ss.login.EmailID, ss.login.RemeberMe, timeout);
                        Session["Type"] = "Entreprise";
                       

                        string encrypted = FormsAuthentication.Encrypt(ticket);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);

                        if (Url.IsLocalUrl(Returnurl))
                        {
                            return Redirect(Returnurl);
                        }
                        else
                        {
                            return RedirectToAction("ProfileEntreprise", "Entreprise", new { id = x.Entreprise_Id });
                        }
                    }
                    else { message = "Verify your account first"; }
                }
                else { message = "Invalid Password"; }
            }
            else { message = "Invalide Email "; }
            TempData["MessageLogin"] = message;
            return RedirectToAction("LoginRegister", "Signup");
        }

        [HttpGet]
        public ActionResult VerifiyAccount(string id)
        {
            var x = ES.GetAll().Where(a => a.ActivationCode.ToString() == id).FirstOrDefault();

            if (x != null)
            {
                Entreprise ent = ES.GetAll().Where(a => a.ActivationCode.ToString() == id).First();
                ent.IsEmailVerified = true;
                ES.Update(ent);
                ES.Commit();
                return View();

            }
            else

                return RedirectToAction("LoginRegister", "Login");
        }

        /*****************************************************/


        public ActionResult Index()
        {
            return View ("~/Views/Login/Index.cshtml");
        }

        public ActionResult Details(int id)
        {
            return View ();
        }

        public ActionResult Create()
        {
            return View ();
        } 

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try {
                return RedirectToAction ("Index");
            } catch {
                return View ();
            }
        }
        
        public ActionResult Edit(int id)
        {
            return View ();
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try {
                return RedirectToAction ("Index");
            } catch {
                return View ();
            }
        }

        public ActionResult Delete(int id)
        {
            return View ();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try {
                return RedirectToAction ("Index");
            } catch {
                return View ();
            }
        }
    }
}