﻿using Domain.Entities;
using PI.Service;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB.Controllers
{
    public class InterviewController : Controller
    {
        InterviewService Is;
        CandidatService Ps;
        ProjectManagerService CS;
        EmailService Ms;
        public InterviewController()
        {
            Is = new InterviewService();
            Ps = new CandidatService();
            CS = new ProjectManagerService();
            Ms = new EmailService();

        }
        // GET: Interview
        public ActionResult Index()
        {
            return View(Is.GetMany());
        }

        // GET: Interview/Details/5
        public ActionResult Details(int id)
        {
            return View(Is.GetById(id));
        }
        [HttpPost]
        public ActionResult Index(string filtre)
        {
            var list = Is.GetMany();


            // recherche
            if (!String.IsNullOrEmpty(filtre))
            {
                list = list.Where(m => m.Interview_Room.ToString().Contains(filtre)).ToList();
            }

            return View(list);



        }

        // GET: Interview/Create
        public ActionResult Create()
        {
            var candidats = Ps.GetMany();
            ViewBag.Candidat_Id = new SelectList(candidats, "Candidat_Id", "Candidat_FullName");

            var projectmanagers = CS.GetMany();
            ViewBag.ProjectManager_Id = new SelectList(projectmanagers, "ProjectManager_Id", "ProjectManager_FullName");

            return View();
        }

        // POST: Interview/Create
        [HttpPost]
        public ActionResult Create(Interview p)
        {
            Is.Add(p);
            Is.Commit();
            Candidat C = Ps.GetById(p.Candidat_Id);

            EmailService Ms = new EmailService();
            Ms.receiver_Email = C.Candidat_Email;
     
            Ms.receiver_FullName = C.Candidat_FullName;
            Ms.Mail_Subject = "Availablity";
            Ms.Mail_Body = "you have to check your interview's date";
            Ms.SendEmail();
            


            return RedirectToAction("index");
            
        }

        // GET: Interview/Edit/5
        public ActionResult Edit(int id)
        {
            Interview v = Is.GetById(id);
            if (v == null)
            {
                return HttpNotFound();
            }
            return View(Is.GetById(id));
        }

        // POST: Interview/Edit/5
        [HttpPost]
        public ActionResult Edit(Interview p)
        {
            Interview p1 = Is.GetById(p.Interview_Id);
            p1.Interview_Date = p.Interview_Date;
            p1.Interview_Object = p.Interview_Object;
            p1.Interview_Description = p.Interview_Description;
            p1.Interview_Room = p.Interview_Room;
            p1.Candidat_Availibility = p.Candidat_Availibility;
            p1.ProjectManager_Availibility = p.ProjectManager_Availibility;
            if (ModelState.IsValid)
            {
                Is.Update(p1);
                Is.Commit();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Interview/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Is.GetById(id));
        }

        // POST: Interview/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Interview p)
        {
            p = Is.GetById(id);
            Is.Delete(p);
            Is.Commit();
            return RedirectToAction("Index");
        }
    }
}
