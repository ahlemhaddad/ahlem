﻿using Domain.Entities;
using PI.Service;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB.Controllers
{
    public class ValidationController : Controller
    {
        ValidationService Ps;
        CandidatService CS;
        OffreService OS;
        public ValidationController()
        {
            Ps = new ValidationService();
            CS = new CandidatService();
            OS = new OffreService();
        }
        [Authorize]
        public ActionResult Index()
        {
            return View(Ps.GetMany());
        }
        // GET: Validation/Details/5
        public ActionResult Details(int id)
        {
            return View(Ps.GetById(id));
        }
        /*[HttpPost]
        public ActionResult Index(string filtre)
        {
            var list = Ps.GetMany();


            // recherche
            if (!String.IsNullOrEmpty(filtre))
            {
                list = list.Where(m => m.Validateur_Name.ToString().Contains(filtre)).ToList();
            }

            return View(list);



        }*/

        // GET: Validation/Create
        public ActionResult Create()
        { 
            //test git
            var candidats = CS.GetMany();
            ViewBag.Candidat_Id = new SelectList(candidats, "Candidat_Id", "Candidat_FullName");

            var offres = OS.GetMany();
            ViewBag.Offre_Id = new SelectList(offres, "Offre_Id", "Offre_Name");

            return View();
        }

        // POST: Validation/Create
        [HttpPost]
        public ActionResult Create(Validation p)
        {
            Ps.Add(p);
            Ps.Commit();
            return RedirectToAction("index");
        }

        // GET: Validation/Edit/5
        public ActionResult Edit(int id)
        {
            Validation v = Ps.GetById(id);
            if (v == null)
            {
                return HttpNotFound();
            }
            return View(v);
        }

        // POST: Validation/Edit/5
        [HttpPost]
        public ActionResult Edit( Validation p)
        {
            Validation p1 = Ps.GetById(p.Validation_Id);
            p1.Validation_State = p.Validation_State;
        
            p1.Validation_desc = p.Validation_desc;
            if (ModelState.IsValid) { 
            Ps.Update(p1);
            Ps.Commit();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        // GET: Validation/Delete/5
        public ActionResult Delete(int id)
        {
            return View(Ps.GetById(id));
        }

        // POST: Validation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Validation p)
        {
            p = Ps.GetById(id);
            Ps.Delete(p);
            Ps.Commit();
            return RedirectToAction("Index");
        }
    }
}
