﻿using Domain.Entities;
using PI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WEB.Models;

namespace WEB.Controllers
{
    public class EntrepriseController : Controller
    {
        EntrepriseService ES;

        public EntrepriseController()
        {
            ES = new EntrepriseService();
        }


        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult ProfileEntreprise(string id)
        {
           
            Entreprise ent = ES.GetAll().Where(a => a.Entreprise_Email == User.Identity.Name).First();

            var entreprise = ES.GetAll().Where(a => a.Entreprise_Id.ToString() == id);

            if (entreprise == null)
            {
                return RedirectToAction("Index", "Entreprise");
            }


            Entreprise entProfile = entreprise.First();


            if (ent.Entreprise_Id.ToString() == id)
            {
                ViewBag.Status = true;
                return View(new EntrepriseModel
                {
                    Entreprise = entProfile

                });
            }
            else
            {
                ViewBag.Status = false;
                return View(new EntrepriseModel
                {
                    Entreprise = entProfile

                });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateEntreprise(EntrepriseModel em)
        {


            if (ModelState.IsValidField("Entreprise_Overview"))
            {
                Entreprise ent = ES.GetAll().Where(a => a.Entreprise_Email == User.Identity.Name).First();
                
                if (Request["CityCountryForm"] == "true")
                {
                    ent.Entreprise_Country = em.Entreprise.Entreprise_Country;
                    ent.Entreprise_City = em.Entreprise.Entreprise_City;
                }
                if (Request["OverviewForm"] == "true")
                    ent.Entreprise_Overview = em.Entreprise.Entreprise_Overview;
                if (Request["TotalEmployesForm"] == "true")
                    ent.Entreprise_TotalEmployes = em.Entreprise.Entreprise_TotalEmployes;
                if (Request["DateForm"] == "true")
                    ent.Entreprise_Date = em.Entreprise.Entreprise_Date;







                em = new EntrepriseModel { Entreprise = ent };

                ES.Update(em.Entreprise);
                ES.Commit();
                ViewBag.Info = true;
                return RedirectToAction("ProfileEntreprise", "Entreprise", new { id = em.Entreprise.Entreprise_Id });
            }
            else {
                ViewBag.Info = false;
                return RedirectToAction("ProfileEntreprise", "Entreprise", new { id = em.Entreprise.Entreprise_Id });

            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("LoginRegister", "Login");
        }





    }
}