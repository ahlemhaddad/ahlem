﻿using Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service.Pattern
{
    public class Service<T> : IService<T> where T : class
    {
        IUnitOfWork utk;
        public Service(IUnitOfWork utk)
        {
            this.utk = utk;
        }
        public void Add(T entity)
        {
            utk.getRepository<T>().Add(entity);
        }
        public virtual IEnumerable<T> GetAll()
        {
            return utk.getRepository<T>().GetALL();
            //return _repository.GetById(id);
            //  return utwk.getRepository<TEntity>().GetById(id);
        }

        public void Delete(Expression<Func<T, bool>> where)
        {
            utk.getRepository<T>().Delete(where);
        }

        public void Delete(T entity)
        {
            utk.getRepository<T>().Delete(entity);
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return utk.getRepository<T>().Get(where);
        }

        public T GetById(string id)
        {
            return utk.getRepository<T>().GetById(id);
        }
        public T GetById(int id, int PublicationId, string Description)
        {
            return utk.getRepository<T>().GetById(id, PublicationId, Description);
        }



        public T GetById(long id)
        {
            return utk.getRepository<T>().GetById(id);
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> where = null, Expression<Func<T, bool>> orderBy = null)
        {
            return utk.getRepository<T>().GetMany(where, orderBy);
        }

        public void Update(T entity)
        {
            utk.getRepository<T>().Update(entity);
        }
        public void Commit()
        {
            utk.Commit(); 
           
        }
        public void Dispose()

        {
            utk.Dispose();
        }
    }
}
