﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public enum Countries
    {
        Tunisia ,
        Algeria ,
        Morroco , 
        Libya
    }
    public class Candidat
    {
        public int Candidat_Id { get; set; }
        [Required(ErrorMessage ="this field is required")]
        public string Candidat_FullName { get; set; }
        [Required(ErrorMessage = "this field is required")]
        public string Candidat_Password { get; set; }
        [Required(ErrorMessage = "this field is required")]
        public string Candidat_Email { get; set; }
        public string Candidat_ShortBio { get; set; }
        public string Candidat_LongBio { get; set; }
        public string Candidat_RegisteredDate { get; set; }
        public Countries Candidat_Country { get; set; }
        public string Candidat_Token { get; set; }
        public string Candidat_Confirmed { get; set; }



        public ICollection<Certification>  Certifications { get; set; }
        public ICollection<Formation> Formations { get; set; }
        public ICollection<Validation> Validations { get; set; }
        public ICollection<Interview> Interviews { get; set; }

    }

}
