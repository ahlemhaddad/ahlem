﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Offre
    {
        public int Offre_Id { get; set; }
        public string Offre_Name { get; set; }
        public ICollection<Validation> Validations { get; set; }
    }
}
