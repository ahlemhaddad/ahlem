﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Certification
    {
        public int Cerfication_Id { get; set; }
        public DateTime Cerfication_Date { get; set; }
        public string Cerfication_Link { get; set; }
        public string Cerfication_Name { get; set; }

        public int Candidat_Id { get; set; }
        [ForeignKey("Candidat_Id")]
        public virtual Candidat Candidat { get; set; }




    }
}
