﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ProjectManager
    {
        public int ProjectManager_Id { get; set; }
        public string ProjectManager_FullName { get; set; }
        public ICollection<Interview> Interviews { get; set; }
    }
}
