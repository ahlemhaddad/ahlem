﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class Validation
    {
        public int Validation_Id { get; set; }
        public string Validation_desc { get; set; }
        public bool Validation_State { get; set; }

        //[Required(ErrorMessage = "Validateur_Name is required")]
        public string Validateur_Name { get; set; }
        
        public int Candidat_Id { get; set; }
        [ForeignKey("Candidat_Id")]
        public virtual Candidat Candidat { get; set; }
        public int Offre_Id { get; set; }
        [ForeignKey("Offre_Id")]
        public virtual Offre Offre { get; set; }

    }
}
