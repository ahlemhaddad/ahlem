﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Entreprise
    {
        public int Entreprise_Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Is Required")]
        public string Entreprise_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password Is Required")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 characters required")]
        public string Entreprise_Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password Is Required")]
        [DataType(DataType.Password)]
        [Compare("Entreprise_Password", ErrorMessage = "Confirm password and password do not match")]
        public virtual string Entreprise_ConfirmPassword { get; set; }


        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Is Required")]
        public string Entreprise_Email { get; set; }


        public string Entreprise_Country { get; set; }

        public string Entreprise_City { get; set; }

        [MaxLength(5000)]
        public String Entreprise_Overview { get; set; }


        public DateTime Entreprise_Date { get; set; }

        public int Entreprise_TotalEmployes { get; set; }



        public bool IsEmailVerified { get; set; }
        public System.Guid ActivationCode { get; set; }
    }
}
