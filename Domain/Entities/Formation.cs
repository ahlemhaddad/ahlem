﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Formation
    {
        public int Formation_Id { get; set; }
        public string Formation_Name { get; set; }
        public int Formation_StartDate { get; set; }
        public int Formation_EndDate { get; set; }
        public string Formation_Location { get; set; }


        public int Candidat_Id { get; set; }
        [ForeignKey("Candidat_Id")]
        public virtual Candidat Candidat { get; set; }



    }
}
