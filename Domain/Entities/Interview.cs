﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public enum Rooms { Poppy, Bluebell, Cherryblossom , Snowdrop , Daffodil };
    public class Interview
    {
        public int Interview_Id { get; set; }
        [DisplayFormat (DataFormatString = "{ 0:dd MMM yyyy}")]
        [DataType (DataType.Date)]
        public DateTime Interview_Date { get; set; }
        public string Interview_Object { get; set; }
        public string Interview_Description { get; set; }
        public bool Candidat_Availibility { get; set; }
        public bool ProjectManager_Availibility { get; set; }
        public Rooms Interview_Room { get; set; }

        public int Candidat_Id { get; set; }
        [ForeignKey("Candidat_Id")]
        public virtual Candidat Candidat { get; set; }
        
        public int ProjectManager_Id { get; set; }
        [ForeignKey("ProjectManager_Id")]
        public virtual ProjectManager ProjectManager { get; set; }
    }
}
